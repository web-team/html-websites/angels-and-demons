
<div id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?> <?php print $type ?>">

<?php print $picture ?>




<?php if (!$page): ?>
  <h2><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
<?php endif; ?>



  <div class="content">
	
	<?php $img_main = $node->field_img_main[1]; ?>
	
	<?php if ($img_main): ?>
		<?php print $img_main ?>
		
	<?php endif;?>
	
    <?php print $content ?>
  </div>






</div>


