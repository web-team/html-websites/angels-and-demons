﻿<div class="news_item clear-block">

	<?php $img_thumb = $fields['field_img_thumb_value']->content; ?>
	<?php $node_url = url("node/" . $row->nid); ?>
	
	
	<?php if ($img_thumb): ?>

		<?php $img_alt = $fields['field_img_main_alt_value']->content; ?>
		<?php $img_caption = $fields['field_img_main_caption_value']->content; ?>
			
		<a href="<?php print $node_url; ?>"><img src="<?php print $img_thumb; ?>"	
			<?php if ($img_alt): print 'alt="' . $img_alt . '"'; endif; ?>
			<?php if ($img_caption): print ' title="' . $img_caption . '"'; endif; ?>
			/>
		</a>
		
		
	<?php endif; ?>
	
	
	<h3><?php print $fields['title']->content; ?></h3>
	<?php print $fields['teaser']->content; ?>
	
	
	
</div>